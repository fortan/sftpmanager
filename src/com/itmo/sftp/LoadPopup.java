package com.itmo.sftp;

import javax.swing.*;
import java.awt.*;

public abstract class LoadPopup extends SwingWorker {
    private JDialog dialog;

    public LoadPopup() {
        dialog = new JDialog(null, "Dialog", Dialog.ModalityType.APPLICATION_MODAL);
        JProgressBar progressBar = new JProgressBar();
        progressBar.setIndeterminate(true);

        dialog.setResizable(false);
        dialog.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(progressBar, BorderLayout.CENTER);
        panel.add(new JLabel("Please wait......."), BorderLayout.PAGE_START);
        dialog.add(panel);
        dialog.pack();
        dialog.setLocationRelativeTo(null);

        addPropertyChangeListener((evt) -> {
            if (evt.getNewValue() == SwingWorker.StateValue.DONE) {
                dialog.dispose();
            }
        });
    }

    public void setVisible(boolean b) {
        dialog.setVisible(b);
    }
}
