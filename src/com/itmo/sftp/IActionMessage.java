package com.itmo.sftp;

public interface IActionMessage {
    public void openData(String data);
    public String saveDate();
}

