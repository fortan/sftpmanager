package com.itmo.sftp;

import com.jcraft.jsch.*;

import java.io.*;
import java.util.*;

public class Sftp implements ISftp {
    private Session session;
    private ChannelSftp chanelSftp;
    private String user;
    private String password;
    private String host;
    private int port;
    private String homePath;

    public Sftp(String user, String host,  int port, String password) {
        this.user = user;
        this.host = host;
        this.password = password;
        this.port = port;
    }

    public boolean openSession() {
        try {
            JSch jsch = new JSch();
            session = jsch.getSession(user, host, port);
            UserInfo ui = new HandleUserInfo(password);
            session.setUserInfo(ui);
            session.connect();
            return true;
        } catch (JSchException ex) {
            return false;
        }
    }

    public boolean openChanel() {
        try {
            Channel channel = session.openChannel("sftp");
            channel.connect();
            chanelSftp = (ChannelSftp)channel;
            homePath = pwd();
            if (homePath == null) {
                return false;
            }
            return true;
        } catch (JSchException ex) {
            return false;
        }
    }

    public String getHomePath() {
        return homePath;
    }

    public boolean rename(String p1, String p2) {
        try {
            chanelSftp.rename(p1, p2);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Vector ls(String path) {
        try {
            Vector listFiles = chanelSftp.ls(path);
            listFiles.sort((o1, o2) -> {
                    if(o1 instanceof ChannelSftp.LsEntry && o2 instanceof ChannelSftp.LsEntry) {
                        if (((ChannelSftp.LsEntry) o1).getAttrs().isDir() ==
                                ((ChannelSftp.LsEntry) o2).getAttrs().isDir()) {
                            return ((ChannelSftp.LsEntry) o1).getFilename().compareTo(((ChannelSftp.LsEntry) o2).getFilename());
                        }
                        return ((ChannelSftp.LsEntry) o1).getAttrs().isDir() == true ? - 1 : 1;
                    }
                    return 0;
                }
            );

            return listFiles;
        }
        catch(SftpException e) {
            return null;
        }
    }

    public boolean mkdir(String path) {
        try {
            chanelSftp.mkdir(path);
            return true;
        } catch (SftpException e) {
            return false;
        }

    }
    public boolean cd(String path) {
        try{
            if (chanelSftp == null || path == null) {
                return false;
            }

            chanelSftp.cd(path);
            return true;
        }
        catch (SftpException e) {
            return false;
        }
    }

    public StringBuilder openFile(String path) throws Exception {
        try {
            OutputStreamString oss = new OutputStreamString();
            chanelSftp.get(path, oss);
            return oss.getString();
        } catch (SftpException ex)  {
            return null;
        }
    }

    public boolean saveFile(String path, StringBuilder text) {
        try {
            InputStreamString isf = new InputStreamString();
            isf.setCache(text);
            chanelSftp.put(isf, path);
            return true;
        } catch (SftpException ex)  {
            return false;
        }
    }

    public String pwd() {
        try {
            return chanelSftp.pwd();
        } catch (SftpException ex) {
            return null;
        }
    }

    public boolean rm(String path) {
        try {
            chanelSftp.rmdir(path);
            return true;
        } catch (SftpException e) {
            return false;
        }
    }

    public void exit() {
        try {
            chanelSftp.exit();
            session.disconnect();
        } catch (Exception ex) {

        }
    }


    private class HandleUserInfo  implements UserInfo {
        private String password;

        public HandleUserInfo(String password) {
            this.password = password;
        }

        @Override
        public String getPassphrase() {
            return null;
        }

        @Override
        public String getPassword() {
            return password;
        }

        @Override
        public boolean promptPassword(String s) {
            return true;
        }

        @Override
        public boolean promptPassphrase(String s) {
            return false;
        }

        @Override
        public boolean promptYesNo(String s) {
            return true;
        }

        @Override
        public void showMessage(String s) {
        }
    }

    private class OutputStreamString extends OutputStream {
        private StringBuilder cache = new StringBuilder();

        @Override
        public void write(int b) throws IOException {
            cache.append((char) b);
        }

        public void clearCache() {
            cache = new StringBuilder();
        }

        public StringBuilder getString() {
            return cache;
        }
    }

    private class InputStreamString extends InputStream {
        private StringBuilder cache = new StringBuilder();
        private int pointer = 0;

        @Override
        public int read() throws IOException {
            return cache.length() <= pointer ? - 1 : cache.charAt(pointer++);
        }

        public void setCache(StringBuilder str) {
            this.cache = str;
            pointer = 0;
        }
    }
}





