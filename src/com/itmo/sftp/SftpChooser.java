package com.itmo.sftp;

import com.itmo.Manager;
import com.jcraft.jsch.ChannelSftp;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.*;
import javax.swing.plaf.metal.MetalIconFactory;
import javax.swing.table.*;
import java.awt.*;
import java.awt.Component;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Vector;

public class SftpChooser extends JFrame {
    boolean isPopupShow = false;
    String previousValue;
    JComboBox treeComboBox;
    DefaultTableModel dtm;
    ISftp[] sftp_p;
    JButton action = new JButton();
    enum ActionState {OPEN, SAVE}
    ActionState actionState = ActionState.OPEN;
    ArrayList<IActionMessage> listMessage;

    public SftpChooser(Object[] tblheader) {
        listMessage = new ArrayList<>();
        dtm = new DefaultTableModel(null, tblheader);
        JTextField fileName = new JTextField();
        sftp_p = new Sftp[1];
        JTable tbl = new JTable(dtm)  {
            @Override
            public boolean isCellEditable(int row, int column) {
                boolean tmp = isPopupShow;
                isPopupShow = false;
                return getSelectedColumn() == column && getSelectedRow() == row && tmp;
            }

            @Override
            public Class<?> getColumnClass(int column) {
                if (0 < this.getRowCount()) {
                    if (getValueAt(0, column).getClass() == OverrideJLabel.class)
                        return JLabel.class;
                    if (column == 0)
                        return JLabel.class;
                    return getValueAt(0, column).getClass();
                } else {
                    return null;
                }
            }
        };

        tbl.getDefaultEditor(Object.class).addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(ChangeEvent e) {
                Object obj = tbl.getValueAt(tbl.getSelectedRow(), 0);
                LoadPopup worker = new LoadPopup() {
                    @Override
                    protected Object doInBackground() throws Exception {
                        if (!sftp_p[0].rename(previousValue, ((JLabel) obj).getText())) {
                            SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not rename file", "PANIC", JOptionPane.ERROR_MESSAGE));
                        }

                        return null;
                    }
                };
                worker.execute();
                worker.setVisible(true);

                fillRows(".");
            }

            @Override
            public void editingCanceled(ChangeEvent e) {
                //http://bugs.java.com/bugdatabase/view_bug.do?bug_id=6788481 -> i love java
            }
        });

        JPopupMenu popup = new JPopupMenu();

        JMenuItem newDir = new JMenuItem("New dir");
        newDir.addActionListener(e -> {
            String dirName = JOptionPane.showInputDialog("Enter name directory");
            if (dirName != null) {
                LoadPopup worker = new LoadPopup() {
                    @Override
                    protected Object doInBackground() throws Exception {
                        if (!sftp_p[0].mkdir(dirName)) {
                            SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not make dir", "PANIC", JOptionPane.ERROR_MESSAGE));
                        }

                        return null;
                    }
                };
                worker.execute();
                worker.setVisible(true);

                fillRows(".");
            }
        });


        JMenuItem update = new JMenuItem("Update");
        update.addActionListener(e -> {
            fillRows(".");
        });

        JMenuItem rename = new JMenuItem("Rename");
        rename.addActionListener(e -> {
            isPopupShow = true;
            tbl.editCellAt(tbl.getSelectedRow(), tbl.getSelectedColumn());
            tbl.getEditorComponent().requestFocus();
        });

        JMenuItem delete = new JMenuItem("Delete");
        delete.addActionListener(e -> {
            Object obj = tbl.getValueAt(tbl.getSelectedRow(), 0);
            LoadPopup worker = new LoadPopup() {
                @Override
                protected Object doInBackground() throws Exception {
                    if (!sftp_p[0].rm(((JLabel) obj).getText())) {
                        SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not remove dir", "PANIC", JOptionPane.ERROR_MESSAGE));
                    }

                    return null;
                }
            };
            worker.execute();
            worker.setVisible(true);

            fillRows(".");
        });

        popup.add(newDir);
        popup.add(update);
        popup.add(rename);
        popup.add(delete);


        tbl.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent event) {
                if(event.getButton() == MouseEvent.BUTTON3) {
                    int column = tbl.columnAtPoint(event.getPoint());
                    int row = tbl.rowAtPoint(event.getPoint());
                    if (column != -1 && row != -1) {
                        tbl.setColumnSelectionInterval(column, column);
                        tbl.setRowSelectionInterval(row, row);
                    }
                }

                if (event.getButton() == MouseEvent.BUTTON1 && event.getClickCount() == 2) {
                    Object obj = tbl.getValueAt(tbl.getSelectedRow(), 0);

                    String icon1 = ((JLabel) obj).getIcon().toString();
                    icon1 = icon1.substring(0, icon1.indexOf('@'));
                    String icon2 = MetalIconFactory.getTreeFolderIcon().toString();
                    icon2 = icon2.substring(0, icon2.indexOf('@'));

                    if (icon1.equals(icon2))
                    {
                        changeDir(((JLabel) obj).getText());
                        fillRows(".");
                    }
                    else
                    {
                        fileName.setText(((JLabel) obj).getText());
                    }

                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON3) {
                    int r = tbl.rowAtPoint(e.getPoint());
                    if (r >= 0 && r < tbl.getRowCount()) {
                        tbl.setRowSelectionInterval(r, r);
                    } else {
                        tbl.clearSelection();
                    }

                    if (tbl.getSelectedRow() < 0)
                        return;
                    if (e.getComponent() instanceof JTable) {
                        if (tbl.getSelectedColumn() == 0) {
                            previousValue = new String(((JLabel)tbl.getValueAt(r, 0)).getText());
                            popup.show(e.getComponent(), e.getX(), e.getY());
                        }
                    }
                }
            }
        });

        tbl.setCellSelectionEnabled(false);
        tbl.setDefaultRenderer(JLabel.class, new LabelRenderer());
        tbl.setColumnSelectionAllowed(false);
        tbl.setRowSelectionAllowed(true);
        tbl.setShowGrid(false);
        tbl.setBackground(Color.white);
        tbl.setGridColor(Color.white);
        tbl.setFillsViewportHeight(true);

        JPanel top = new JPanel();
        top.setBorder(new CompoundBorder(new EmptyBorder(15, 15, 15, 15), null));
        top.setLayout(new BoxLayout(top, BoxLayout.X_AXIS));
        top.add(new JLabel("Look in: "));
        treeComboBox = new JComboBox();
        treeComboBox.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (treeComboBox.getSelectedIndex() < treeComboBox.getItemCount() - 1) {
                    int indexSelected = treeComboBox.getSelectedIndex();
                    String path = "";
                    for (int i = 1; i <= indexSelected; i++) {
                        String a = (String) treeComboBox.getItemAt(i);
                        a = a.substring(2 * i);
                        path += "/" + a;
                    }
                    System.out.println(treeComboBox.getSelectedIndex() == 0 ? "/." : path);
                    changeDir(treeComboBox.getSelectedIndex() == 0 ? "/" : path);
                    fillRows(".");
                }
            }
        });
        top.add(treeComboBox);

        JButton upToFolder = new JButton(MetalIconFactory.getFileChooserUpFolderIcon());
        upToFolder.addActionListener(e -> {
            changeDir("..");
            fillRows(".");
        });

        JButton goToHome = new JButton(MetalIconFactory.getFileChooserHomeFolderIcon());
        goToHome.addActionListener(e -> {
            changeDir(sftp_p[0].getHomePath());
            fillRows(".");
        });

        JButton newFolder = new JButton(MetalIconFactory.getFileChooserNewFolderIcon());
        newFolder.addActionListener(newDir.getActionListeners()[0]);

        top.add(upToFolder);
        top.add(goToHome);
        top.add(newFolder);

        JPanel bottom = new JPanel();
        bottom.setLayout(new BoxLayout(bottom, BoxLayout.Y_AXIS));

        JPanel bottom_file = new JPanel();
        bottom_file.setLayout(new BorderLayout());
        bottom_file.setBorder(new CompoundBorder(new EmptyBorder(2, 2, 2, 2), null));
        bottom_file.add(new JLabel("File Name:      "), BorderLayout.WEST);
        bottom_file.add(fileName, BorderLayout.CENTER);

        JPanel bottom_type = new JPanel();
        bottom_type.setLayout(new BorderLayout());
        bottom_type.setBorder(new CompoundBorder(new EmptyBorder(2, 2, 2, 2), null));
        bottom_type.add(new JLabel("Files of Type: "), BorderLayout.WEST);
        JComboBox fileType = new JComboBox();
        fileType.addItem("Any Type");
        bottom_type.add(fileType, BorderLayout.CENTER);

        JPanel btm = new JPanel();
        btm.setLayout(new BorderLayout());

        Box buttonBar = Box.createHorizontalBox();
        buttonBar.setBorder(new CompoundBorder(new EmptyBorder(5, 5, 5, 5), null));
        buttonBar.add(Box.createHorizontalGlue());

        action.addActionListener(e -> {
            switch (actionState) {
                case OPEN:
                    try {
                        LoadPopup worker = new LoadPopup() {
                            @Override
                            protected Object doInBackground() throws Exception {
                                StringBuilder text = sftp_p[0].openFile(fileName.getText());
                                if (text == null) {
                                    SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not open file", "PANIC", JOptionPane.ERROR_MESSAGE));
                                    return null;
                                }

                                for(IActionMessage a: listMessage) {
                                    a.openData(text.toString());
                                }

                                return null;
                            }
                        };
                        worker.execute();
                        worker.setVisible(true);

                    } catch (Exception ex) {

                    }
                    break;

                case SAVE:
                    try {
                        LoadPopup worker = new LoadPopup() {
                            @Override
                            protected Object doInBackground() throws Exception {
                                StringBuilder sb = new StringBuilder();

                                for(IActionMessage a: listMessage) {
                                    sb.append(a.saveDate());
                                }

                                if (!sftp_p[0].saveFile(fileName.getText(), sb))
                                {
                                    SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not save file", "PANIC", JOptionPane.ERROR_MESSAGE));
                                    return null;
                                }

                                return null;
                            }
                        };
                        worker.execute();
                        worker.setVisible(true);

                    } catch (Exception ex) {

                    }
                    break;
            }
            setVisible(false);
        });
        buttonBar.add(action);

        JButton close = new JButton("Cancel");
        close.addActionListener(e -> {
            setVisible(false);
        });

        buttonBar.add(close);

        btm.add(buttonBar);

        bottom.add(bottom_file);
        bottom.add(bottom_type);
        bottom.add(btm);
        bottom.add(buttonBar, BorderLayout.SOUTH);

        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        p.add(new JScrollPane(tbl), BorderLayout.CENTER);
        p.add(top, BorderLayout.NORTH);
        p.add(bottom, BorderLayout.SOUTH);
        add(p);
    }

    @Override
    public void setVisible(boolean b) {
        if (b == true) {

           if (sftp_p[0] != null && !changeDir(sftp_p[0].getHomePath())) {
               return;
           }


           if (!fillRows(sftp_p[0].getHomePath())) {
              return;
           }
        }
        super.setVisible(b);
    }

    public void setSftp(Sftp sftp) {
        sftp_p[0] = sftp;
    }

    private boolean fillRows(String path) {
            clearRows();
            LoadPopup worker = new LoadPopup() {
                @Override
                protected Object doInBackground() throws Exception {
                    if (sftp_p[0] != null) {
                        return sftp_p[0].ls(path);
                    }
                    return null;
                }
            };
            worker.execute();
            worker.setVisible(true);
            Vector v = null;
            try {
                v = (Vector)worker.get();
                if (v == null) {
                    SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not display a list of files", "PANIC", JOptionPane.ERROR_MESSAGE));
                    return false;
                }
            } catch (Exception ex) {
            }


            for (int i = 0; i < v.size(); i++) {
                JLabel fileName = new OverrideJLabel();
                fileName.setText(((ChannelSftp.LsEntry) v.get(i)).getFilename().toString());
                fileName.setOpaque(true);
                if (((ChannelSftp.LsEntry) v.get(i)).getAttrs().isDir()) {
                    fileName.setIcon(MetalIconFactory.getTreeFolderIcon());
                } else {
                    fileName.setIcon(MetalIconFactory.getFileChooserDetailViewIcon());
                }
                long size = ((ChannelSftp.LsEntry) v.get(i)).getAttrs().getSize();
                String modify = ((ChannelSftp.LsEntry) v.get(i)).getAttrs().getMtimeString();
                String perm = ((ChannelSftp.LsEntry) v.get(i)).getAttrs().getPermissionsString();
                Object[] row = {fileName, size, modify, perm};
                dtm.addRow(row);
            }
            repaint();
            return true;
    }

    public boolean changeDir(String path) {
        LoadPopup worker = new LoadPopup() {
            @Override
            protected Object doInBackground() throws Exception {
                if (sftp_p[0] != null && sftp_p[0].cd(path)) {
                    return sftp_p[0].pwd();
                }
                return null;
            }
        };
        worker.execute();
        worker.setVisible(true);
        String path_ = null;
        try {
            path_ = (String)worker.get();
            if (path_ == null) {
                SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not change dir", "PANIC", JOptionPane.ERROR_MESSAGE));
                return false;
            }
        } catch (Exception ex) {

        }

        treeComboBox.removeAllItems();
        String[] a = path_.split("/");
        treeComboBox.addItem("/");
        StringBuilder shift = new StringBuilder();
        for (int i = 1; i < a.length; i++) {
            shift.append("  ");
            treeComboBox.addItem(a.length - 1 == i ? a[i] : shift + a[i]);
        }
        treeComboBox.setSelectedIndex(treeComboBox.getItemCount() - 1);

        return true;
    }

    private void clearRows() {
        dtm.setRowCount(0);
    }

    public void showOpenDialog() {
        action.setText("Open");
        actionState = ActionState.OPEN;
        setVisible(true);
    }

    public void showSaveDialog() {
        action.setText("Save");
        actionState = ActionState.SAVE;
        setVisible(true);
    }

    private class LabelRenderer extends DefaultTableCellRenderer {
        public void fillColor(JTable t,JLabel l,boolean isSelected) {
            if(isSelected){
                l.setBackground(t.getSelectionBackground());
                l.setForeground(t.getSelectionForeground());
            } else {
                l.setBackground(t.getBackground());
                l.setForeground(t.getForeground());
            }
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                       boolean hasFocus, int row, int column) {
            if(value instanceof JLabel) {
                JLabel label = (JLabel)value;
                fillColor(table,label,isSelected);
                return label;
            } else {
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        }
    }

    private class OverrideJLabel extends JLabel {
        @Override
        public String toString() {
            return super.getText();
        }
    }

    public void addAction(IActionMessage e) {
        listMessage.add(e);
    }
}