package com.itmo.sftp;

import java.util.Vector;

public interface ISftp {
    public String getHomePath();
    public boolean rename(String p1, String p2);
    public Vector ls(String path);
    public boolean mkdir(String path);
    public boolean cd(String path);
    public StringBuilder openFile(String path) throws Exception;
    public boolean saveFile(String path, StringBuilder text);
    public String pwd();
    public boolean rm(String path);
}
