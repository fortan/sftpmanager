package com.itmo;

import com.itmo.sftp.IActionMessage;
import com.itmo.sftp.LoadPopup;
import com.itmo.sftp.Sftp;
import com.itmo.sftp.SftpChooser;

import javax.swing.*;
import java.awt.*;

public class Manager {
    private static Sftp sftp;
    private static SftpChooser chooser;
    public static JTextArea text;

    public static void main(String[] args) {
        chooser = new SftpChooser(new String[]{"Name", "Size", "Modify data", "Perm"});
        JFrame frame = new JFrame();
        JButton run = new JButton("Compile");
        run.setEnabled(false);
        JButton connect = new JButton("Connect");
        JButton open = new JButton("Open");

        chooser.addAction(new IActionMessage() {
              @Override
              public void openData(String data) {
                  if (data != null) {
                      text.setText(data);
                  }
              }

              @Override
              public String saveDate() {
                  return text.getText();
              }
          }
        );

        open.addActionListener(e -> {
            chooser.showOpenDialog();
            chooser.setSize(600, 400);
            chooser.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            chooser.setLocation((int) ((Toolkit.getDefaultToolkit().getScreenSize().width - chooser.getWidth()) / 2),
                    (int) (Toolkit.getDefaultToolkit().getScreenSize().height - chooser.getWidth()) / 2);
        });

        JButton save = new JButton("Save");
        save.addActionListener(e -> {
            chooser.showSaveDialog();
            chooser.setSize(600, 400);
            chooser.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            chooser.setLocation((int) ((Toolkit.getDefaultToolkit().getScreenSize().width - chooser.getWidth()) / 2),
                    (int) (Toolkit.getDefaultToolkit().getScreenSize().height - chooser.getWidth()) / 2);
        });
        connect.addActionListener(a -> {
            sftp = null;
            String user = JOptionPane.showInputDialog("Enter username");
            if (user != null) {
                Object ob[] = {new JPasswordField()};
                LoadPopup worker = new LoadPopup() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        if (JOptionPane.showConfirmDialog(null, ob, "Enter password", JOptionPane.OK_OPTION) == JOptionPane.OK_OPTION) {
                            String pswd = ((JTextField) ob[0]).getText();
                            sftp = new Sftp(user, "helios.cs.ifmo.ru", 2222, pswd);

                            if (!sftp.openSession()) {
                                SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not open session", "PANIC", JOptionPane.ERROR_MESSAGE));
                                return null;
                            }

                            if (!sftp.openChanel()) {
                                SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "Can not open chanel"));
                                return null;
                            }
                            chooser.setSftp(sftp);
                            SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(null, "The connection is established"));
                        }
                        return null;
                    }
                };
                worker.execute();
                worker.setVisible(true);
            }
        });

        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BorderLayout());
        text = new JTextArea();
        leftPanel.add(new JScrollPane(text));

        JPanel rightPanelGrid = new JPanel();
        rightPanelGrid.setLayout(new GridLayout(0, 1));
        rightPanelGrid.add(run);
        rightPanelGrid.add(connect);
        rightPanelGrid.add(open);
        rightPanelGrid.add(save);

        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        rightPanel.add(rightPanelGrid);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(leftPanel);
        panel.add(rightPanel);

        frame.setTitle("SftpManager");
        frame.add(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize((int) (0.75 * Toolkit.getDefaultToolkit().getScreenSize().width),
                (int) (0.75 * Toolkit.getDefaultToolkit().getScreenSize().height));
        frame.setLocation((int) (0.125 * Toolkit.getDefaultToolkit().getScreenSize().width),
                (int) (0.125 * Toolkit.getDefaultToolkit().getScreenSize().height));
        frame.setVisible(true);
    }
}
